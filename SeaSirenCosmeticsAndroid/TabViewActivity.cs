using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using System.Net;
using System.Json;
using System.Linq;

using SeaSirenCosmeticsAndroid;

namespace WPRestAPI
{
	/// <summary>
	/// Tab view activity used two switch between blog and products
	/// </summary>
	[Activity]
	public class TabViewActivity : TabActivity
	{
		/// <summary>
		/// Called by the OS on creation of the activity
		/// </summary>
		/// <param name="bundle">The bundle with the resources for the Activity</param>
		protected override void OnCreate (Bundle bundle)
		{
			RequestWindowFeature (WindowFeatures.NoTitle);
			base.OnCreate (bundle);

			var host = TabHost;

			// Tab for Blog
			var blogSpec = host.NewTabSpec ("Blog");
			blogSpec.SetIndicator ("Blog", Resources.GetDrawable (Resource.Drawable.icon_blog_tab));
			var blogIntent = new Intent (this, typeof(BlogActivity));
			blogSpec.SetContent (blogIntent);

			// Tab for Product Catalogue
			var productSpec = host.NewTabSpec ("Product Catalogue");
			productSpec.SetIndicator ("Product Catalogue", Resources.GetDrawable (Resource.Drawable.icon_product_catalogue_tab));
			var productIntent = new Intent (this, typeof(ProductCatalogueActivity));
			productSpec.SetContent (productIntent);

			// Tab for Try it on page
			var tryItOnSpec = host.NewTabSpec ("Try It On");
			tryItOnSpec.SetIndicator ("Try It On", Resources.GetDrawable (Resource.Drawable.icon_product_catalogue_tab));
			var tryItOnIntent = new Intent (this, typeof(TryItOnActivity));
			tryItOnSpec.SetContent (tryItOnIntent);

			// Tab for About Page
			var aboutSpec = host.NewTabSpec ("About");
			aboutSpec.SetIndicator ("About", Resources.GetDrawable (Resource.Drawable.DefaultLogoImage));
			var aboutIntent = new Intent(this, typeof(AboutActivity));
			aboutSpec.SetContent (aboutIntent);

			host.AddTab (blogSpec);
			host.AddTab (productSpec);
			host.AddTab (tryItOnSpec);
			host.AddTab (aboutSpec);
		}
	}
}