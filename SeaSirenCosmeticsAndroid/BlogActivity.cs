using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SeaSirenCosmeticsCore;
using WPRestAPI;

namespace SeaSirenCosmeticsAndroid
{
	/// <summary>
	/// Activity for displaying the blog posts from the WordPress site.
	/// </summary>
	[Activity (Label = "BlogActivity")]			
	public class BlogActivity : Activity
	{
		/// <summary>
		/// The Scrollable layout.
		/// </summary>
		private LinearLayout m_ScrollLayout;
		/// <summary>
		/// Local cache of all the posts from the Wordpress Site.
		/// </summary>
		private List<WPPost> m_Posts = new List<WPPost> ();

		/// <summary>
		/// Called by the OS on creation of the activity
		/// </summary>
		/// <param name="bundle">The bundle with the resources for the Activity</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle); 

			SetContentView (Resource.Layout.Blog);
			m_ScrollLayout = FindViewById<LinearLayout> (Resource.Id.linearLayout1);

			var app = AppController.Instance;
			app.NewBlogPostInCacheEvent += AddPost;
			app.BlogPostInCacheUpdatedEvent += AddPost;

			app.GetBlogPosts ((posts) => {
				Console.WriteLine("Got posts in blog : " + posts.Count);
				foreach(var p in posts)
				{
					AddPost (p);
				}
			});
		}

		/// <summary>
		/// Adds a post to the blog layout.
		/// </summary>
		/// <param name="p">The post to add to the blog activity</param>
		private void AddPost(WPPost p)
		{
			if (m_Posts.Contains (p))
				return;

			RunOnUiThread (() => {
				var title = new TextView (this);
				title.Text = p.Title;
				m_ScrollLayout.AddView (title);
				var content = new TextView (this);
				content.Text = p.Content;
				m_ScrollLayout.AddView (content);
			});
		}
	}
}

