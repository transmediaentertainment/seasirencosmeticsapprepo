using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using WPRestAPI;

namespace SeaSirenCosmeticsAndroid
{
	[Activity (Label = "LoadingSplashActivity", MainLauncher = true)]			
	public class LoadingSplashActivity : Activity
	{
		private ProgressBar m_PB;
		protected override void OnCreate (Bundle bundle)
		{
			RequestWindowFeature (WindowFeatures.NoTitle);
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.LoadingSplash);
			m_PB = FindViewById<ProgressBar> (Resource.Id.progressBar1);
			m_PB.Visibility = ViewStates.Visible;

			var t = Task.Run (async delegate {
				await Task.Delay (1000);
				Intent tabIntent = new Intent(this, typeof(TabViewActivity));
				StartActivity(tabIntent);
				Finish();
			});
		}
	}
}

