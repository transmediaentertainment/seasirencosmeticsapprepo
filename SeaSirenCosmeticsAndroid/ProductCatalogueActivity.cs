using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SeaSirenCosmeticsCore;
using WPRestAPI;
using LocalDataCaching;
using SeaSirenCosmetics.Core;

namespace SeaSirenCosmeticsAndroid
{
	/// <summary>
	/// Activity for displaying the Product catalogue posts from the WordPress site.
	/// </summary>
	[Activity (Label = "ProductCatalogueActivity")]			
	public class ProductCatalogueActivity : Activity
	{
		/// <summary>
		/// Array used for populating the category spinner
		/// </summary>
		private ArrayAdapter m_SpinnerAdapter;
		/// <summary>
		/// The Spinner used to select categories.
		/// </summary>
		private Spinner m_Spinner;
		private List<string> m_CatSorter;

		/// <summary>
		/// The Linear Layouts currently in the scrollable portion.
		/// </summary>
		private List<LinearLayout> m_Posts = new List<LinearLayout> ();
		private List<ImageView> m_Images = new List<ImageView> ();
		private List<TextView> m_Texts = new List<TextView> ();
		/// <summary>
		/// The Scrolling layout populated with the products.
		/// </summary>
		private LinearLayout m_ScrollingLayout;

		/// <summary>
		/// Called by the OS on creation of the activity
		/// </summary>
		/// <param name="bundle">The bundle with the resources for the Activity</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ProductCatalogue);

			m_Spinner = FindViewById<Spinner> (Resource.Id.spinner1);
			m_Spinner.ItemSelected += HandleItemSelected;

			m_ScrollingLayout = FindViewById<LinearLayout> (Resource.Id.productInnerLinearLayout);

			m_CatSorter = new List<string> ();
			m_CatSorter.Add ("Select");
			m_SpinnerAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleSpinnerDropDownItem, m_CatSorter);
			m_Spinner.Adapter = m_SpinnerAdapter;
			//m_SpinnerAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			AppController.Instance.Products.GetCategories ((categories) => {
				Console.WriteLine("Received Categories : " + categories.Count);
				RunOnUiThread(() => {
					foreach (var cat in categories) 
					{
						m_SpinnerAdapter.Add(cat.Name);
					}
					m_SpinnerAdapter.NotifyDataSetChanged();
				});
			});

			AppController.Instance.Products.NewCategoryAdded += HandleNewCategoryAdded;
		}

		/// <summary>
		/// Handles the new category added event from the Product Cache
		/// </summary>
		/// <param name="cat">Cat.</param>
		private void HandleNewCategoryAdded(WPCategory cat)
		{
			if(m_CatSorter.Contains(cat.Name))
				return;
			Console.WriteLine ("Handling New Category Added : " + cat.Name);
			RunOnUiThread (() => {
				m_SpinnerAdapter.Add (cat.Name);
				m_SpinnerAdapter.NotifyDataSetChanged();
			});
		}

		/// <summary>
		/// Handles an item being selected in the drop down category list.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event arguments</param>
		private void HandleItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Console.WriteLine ("Handling Item Selected");
			// If our spinner fired this event
			if (sender == m_Spinner) 
			{
				var selection = m_Spinner.GetItemAtPosition (e.Position);
				Console.WriteLine("Spinner Selection : " + selection);
				UpdateToCategory (string.Format("{0}", selection));
			}
		}

		/// <summary>
		/// Updates the scrollable list to the latest category selection
		/// </summary>
		/// <param name="cat">Cat.</param>
		private void UpdateToCategory(string cat)
		{
			Console.WriteLine ("Updating to category : " + cat);
			AppController.Instance.Products.GetProductsInCategory (cat, (posts) => {
				Console.WriteLine ("Found " + posts.Count + " posts in category : " + cat);
				RunOnUiThread(() => {
					// Remove current ones
					for(int i = 0; i < m_Posts.Count; i++)
					{
						var v = m_Posts [i];
						v.RemoveView (m_Texts [i]);
						v.RemoveView (m_Images [i]);
						m_ScrollingLayout.RemoveView (v);
					}

					m_Posts.Clear ();
					m_Images.Clear ();
					m_Texts.Clear ();

					foreach (var p in posts) 
					{
						AddProductToListings (p);
					}
				});
			});
		}

		/// <summary>
		/// Adds the product to scrollable view
		/// </summary>
		/// <param name="post">Post to add to the list</param>
		private void AddProductToListings(WPPost post)
		{
			var layout = new LinearLayout (this);
			layout.Orientation = Orientation.Horizontal;

			var imageView = new ImageView (this);
			var imageWrapper = new TTImage(post.FeaturedImage);
			var thumb = imageWrapper.GetThumbnail (64, 64);
			if (thumb != null)
			{
				imageView.SetImageBitmap (thumb.Image);
			}
			else
			{
				imageView.SetImageDrawable (Resources.GetDrawable(Resource.Drawable.DefaultLogoImage));
			}

			layout.AddView (imageView);
			m_Images.Add (imageView);

			var text = new TextView (this);
			text.Text = post.Content;

			layout.AddView (text);
			m_Texts.Add (text);

			m_ScrollingLayout.AddView (layout);
			m_Posts.Add (layout);
		}
	}
}

