<?php

class woocommerce_Jetpack_REST_API {
	private static $instance;

	public static function instance() {
		if( isset(self::$instance) )
			return self::$instance;

		self::$instance = new self;
	}

	private function __construct() {
		add_filter ( 'rest_api_allowed_public_metadata', array ($this, 'allow_woocommerce_public_metadata') );
	}

	function allow_woocommerce_public_metadata ( $allowed_meta_keys ) {
		if ( ! defined( 'REST_API_REQUEST' ) || ! REST_API_REQUEST )
			return $allowed_meta_keys;

		$allowed_meta_keys[] = '_visibility';
		$allowed_meta_keys[] = '_stock_status';
		$allowed_meta_keys[] = 'total_sales';
		$allowed_meta_keys[] = '_product_image_gallery';
		$allowed_meta_keys[] = '_regular_price';
		$allowed_meta_keys[] = '_sale_price';
		$allowed_meta_keys[] = '_tax_status';
		$allowed_meta_keys[] = '_tax_class';
		$allowed_meta_keys[] = '_purchase_note';
		$allowed_meta_keys[] = '_featured';
		$allowed_meta_keys[] = '_weight';
		$allowed_meta_keys[] = '_length';
		$allowed_meta_keys[] = '_width';
		$allowed_meta_keys[] = '_height';
		$allowed_meta_keys[] = '_sku';
		$allowed_meta_keys[] = '_product_attributes';
		$allowed_meta_keys[] = '_sale_price_dates_from';
		$allowed_meta_keys[] = '_sale_price_dates_to';
		$allowed_meta_keys[] = '_price';
		$allowed_meta_keys[] = '_sold_individually';
		$allowed_meta_keys[] = '_stock';
		$allowed_meta_keys[] = '_backorders';
		$allowed_meta_keys[] = '_upsell_ids';
		$allowed_meta_keys[] = '_crosssell_ids';

		return $allowed_meta_keys;
	}

}

woocommerce_Jetpack_REST_API::instance();