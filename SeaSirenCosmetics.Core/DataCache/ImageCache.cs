using System;
using System.IO;
using System.Net;
using System.Drawing;
#if __ANDROID__
using Android.Graphics;
#else
using MonoTouch.UIKit;
#endif

namespace LocalDataCaching
{
	/// <summary>
	/// Image cache class
	/// </summary>
	public static class ImageCache
	{
		/// <summary>
		/// Occurs when system has finished saving an image.
		/// </summary>
		public static event Action<string> FinishedSavingImage;

		/// <summary>
		/// Caches the image at the given URL if not already cached.
		/// </summary>
		/// <param name="url">The URL of the image to cache</param>
		public static void CacheImageFromURL(string url)
		{
			var path = GetLocalImagePath (url);
			if(File.Exists(path))
			{
				// Already have the image cached!
				return;
			}

			var webClient = new WebClient();
			webClient.DownloadDataCompleted += (sender, e) => {
				if(e.Cancelled)
				{
					Console.WriteLine("Download was Cancelled : " + sender.ToString());
				}
				else if(e.Error != null)
				{
					Console.WriteLine("Error in download : " + e.Error.ToString());
				}
				else
				{
					var stream = File.Create(path);
					var bytes = e.Result;
					stream.Write(bytes, 0, bytes.Length);
					stream.Close();
#if !__ANDROID__
					// TODO : Save resized image on iOS

#endif
					if(FinishedSavingImage != null)
						FinishedSavingImage(path);
				}
			};

			//Console.WriteLine("Beginning download of : " + url);
			webClient.DownloadDataAsync(new Uri(url));
		}

		/// <summary>
		/// Gets the local image path for the image's url.
		/// </summary>
		/// <returns>The image path for the cached image.  Returns null if no image is available in Cache</returns>
		/// <param name="imageUrl">The internet URL of the image required</param>
		public static string GetImagePathForCache(string imageUrl)
		{
			var path = GetLocalImagePath (imageUrl);
			if (!File.Exists (path))
				return null;

			return path;
		}

		/// <summary>
		/// Takes the web site Image URL and converts it into a local disk path
		/// </summary>
		/// <returns>The local image path.</returns>
		/// <param name="imageUrl">Image URL.</param>
		private static string GetLocalImagePath(string imageUrl)
		{
			var splits = imageUrl.Split('/', '\\');
			string path;
#if __ANDROID__
			path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Images");
#else
			// On iOS, we will need to cache in the Library folder to meet Apple's iCloud terms.
			var docsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			path = Path.Combine(docsPath, "..", "Library");
#endif
			if(!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			path = System.IO.Path.Combine(path, splits[splits.Length - 1]);

			return path;
		}


#if !__ANDROID__
		/// <summary>
		/// Caches the image to disk
		/// </summary>
		/// <param name="image">The UIImage to cache</param>
		/// <param name="filePath">File path to store the image at</param>
		/// <param name="asPNG">If set to <c>true</c> as PNG, otherwise as a JPEG</param>
		public static void CacheImage(UIImage image, string filePath, bool asPNG = true)
		{
			MonoTouch.Foundation.NSError err;
			Console.WriteLine("Saving thumbnail to : " + filePath);
			if(asPNG)
			{
				using (var data = image.AsPNG()) 
				{
					data.Save (filePath, true, out err);
				}
			}
			else 
			{
				using (var data = image.AsJPEG()) 
				{
					data.Save (filePath, true, out err);
				}
			}
			if (err != null) 
			{
				Console.WriteLine ("Error saving image to disk : " + err.LocalizedDescription);
			}
		}
#endif
	}
}

