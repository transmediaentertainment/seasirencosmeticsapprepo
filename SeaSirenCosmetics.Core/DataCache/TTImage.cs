using System;
using LocalDataCaching;

#if __ANDROID__
using Android.Graphics;
#else
using MonoTouch.UIKit;
using System.IO;
using System.Drawing;
#endif

namespace SeaSirenCosmetics.Core
{
	/// <summary>
	/// Image wrapper to treat Images the same on iOS and Android
	/// </summary>
	public class ImageWrapper
	{
#if __ANDROID__
		public Bitmap Image { get; set; }
#else
		public UIImage Image { get; set; }
#endif
	}

	/// <summary>
	/// TransTech Image class.  A wrapper/facade class for easy handling of images across polatforms
	/// </summary>
	public class TTImage
	{
		/// <summary>
		/// The Web URL of the Image.
		/// </summary>
		/// <value>The URL</value>
		public string URL { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmetics.Core.TTImage"/> class.
		/// </summary>
		/// <param name="imageURL">Image URL (from the web)</param>
		public TTImage (string imageURL)
		{
			URL = imageURL;
		}

		/// <summary>
		/// Gets the image.
		/// </summary>
		/// <returns>The image as an ImageWrapper object, or null if it isn't cached yet</returns>
		public ImageWrapper GetImage()
		{
			// If we haven't cached the image yet, return null
			var path = ImageCache.GetImagePathForCache (URL);
			if (path == null)
				return null;

			var wrapper = new ImageWrapper ();
#if __ANDROID__
			wrapper.Image = BitmapFactory.DecodeFile(path);
#else
			wrapper.Image = new UIImage(path);
#endif
			return wrapper;
		}

		/// <summary>
		/// Gets the thumbnail version of the image
		/// </summary>
		/// <returns>The thumbnail sized image, or null if not cached yet</returns>
		/// <param name="maxWidth">Max width for the thumbnail</param>
		/// <param name="maxHeight">Max height for the thumbnail</param>
		public ImageWrapper GetThumbnail(int maxWidth, int maxHeight)
		{
			// If the image isn't cached yet, return null
			var path = ImageCache.GetImagePathForCache (URL);
			if (path == null)
				return null;

			var wrapper = new ImageWrapper ();

#if __ANDROID__
			// Resize and return the image
			var options = new BitmapFactory.Options { InJustDecodeBounds = true };
			BitmapFactory.DecodeFile(path, options);
			var height = (float) options.OutHeight;
			var width = (float) options.OutWidth;
			var inSampleSize = 1D;

			if(height > maxHeight || width > maxWidth)
			{
				inSampleSize = width > height ? height / maxHeight : width / maxWidth;
			}

			options.InSampleSize = (int)inSampleSize;
			options.InJustDecodeBounds = false;
			wrapper.Image = BitmapFactory.DecodeFile(path, options);
#else
			// Resize and return the image
			var split = path.Split('.');
			split[split.Length - 2] += "_thumb_"+ maxWidth + "x" + maxHeight;

			string thumbPath = "";
			for(int i = 0; i < split.Length; i++)
			{
				thumbPath += split[i];
				if(i < split.Length - 1)
					thumbPath += ".";
			}
			if(!File.Exists(thumbPath))
			{
				var img = new UIImage(path);
				wrapper.Image = MaxResizeImage (img, maxWidth, maxHeight);
				if(split[split.Length - 1] == "jpg" || split[split.Length - 1] == "jpeg")
					ImageCache.CacheImage(wrapper.Image, thumbPath, false);
				else
					ImageCache.CacheImage(wrapper.Image, thumbPath);
			}
			else
			{
				Console.WriteLine("Loading cached Thumbnail : " + thumbPath);
				wrapper.Image = new UIImage(thumbPath);
			}
#endif
			return wrapper;
		}

#if !__ANDROID__
		/// <summary>
		/// Resizes the image to the maximum size based on parameters
		/// </summary>
		/// <returns>The resized image.</returns>
		/// <param name="sourceImage">Source image.</param>
		/// <param name="maxWidth">Max width.</param>
		/// <param name="maxHeight">Max height.</param>
		private UIImage MaxResizeImage(UIImage sourceImage, float maxWidth, float maxHeight)
		{
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max (maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
			if (maxResizeFactor > 1)
				return sourceImage;
			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;
			UIGraphics.BeginImageContext (new SizeF(width, height));
			sourceImage.Draw (new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return resultImage;
		}
#endif
	}
}

