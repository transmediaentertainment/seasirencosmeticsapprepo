using System;
using System.Collections.Generic;
using SQLite;
using WPRestAPI;
using System.IO;
using System.Linq.Expressions;

namespace LocalDataCaching
{
	/// <summary>
	/// Class for storing Products in an SQLite database
	/// </summary>
	public class ProductCache : SQLiteAsyncConnection
	{
		/// <summary>
		/// The name of the DB the products are cached in
		/// </summary>
		public const string DBName = "products.db";
		/// <summary>
		/// Platform dependant path of the database file.
		/// </summary>
		/// <value>The DB path.</value>
		public static string DBPath { get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DBName); } }

		/// <summary>
		/// Occurs when new post added to the Database
		/// </summary>
		public event Action<WPPost> NewPostAdded;
		/// <summary>
		/// Occurs when post was updated in the database
		/// </summary>
		public event Action<WPPost> PostWasUpdated;
		/// <summary>
		/// Occurs when new category added to the database
		/// </summary>
		public event Action<WPCategory> NewCategoryAdded;
		/// <summary>
		/// Occurs when category was updated in the database
		/// </summary>
		public event Action<WPCategory> CategoryWasUpdated;

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalDataCaching.ProductCache"/> class.
		/// </summary>
		public ProductCache() : base(DBPath)
		{
			CreateTablesAsync<WPPost, WPAuthor, WPCategory, WPTag> ();

			Console.WriteLine("DB now open from : " + DBPath);
		}

		/// <summary>
		/// Retrieves all posts in the database.
		/// </summary>
		/// <returns>All posts in the database</returns>
		public void GetAllPosts(Action<List<WPPost>> OnCompletion)
		{
			QueryAsync<WPPost>("select * from WPPost").ContinueWith ((t) => {
				OnCompletion(t.Result);
			});
		}

		/// <summary>
		/// Retrieves all posts in the database from the Blog (type = post)
		/// </summary>
		/// <returns>The blog posts in the database</returns>
		public void GetBlogPosts(Action<List<WPPost>> OnCompletion)
		{
			QueryAsync<WPPost> ("select * from WPPost where Type='post'").ContinueWith ((t) => {
				OnCompletion(t.Result);
			});
		}

		/// <summary>
		/// Retrieves all Product Posts in the database
		/// </summary>
		/// <returns>The product posts in the database</returns>
		public void GetProductPosts(Action<List<WPPost>> OnCompletion)
		{
			QueryAsync<WPPost> ("select * from WPPost where Type='product'").ContinueWith ((t) => {
				OnCompletion(t.Result);
			});
		}

		/// <summary>
		/// Retrieves all product posts in a given category in the database
		/// </summary>
		/// <returns>The product post in the category.</returns>
		/// <param name="cat">The category to get products from.</param>
		public void GetProductsInCategory(string cat, Action<List<WPPost>> OnCompletion)
		{
			QueryAsync<WPPost> (string.Format("select * from WPPost where Type='product' and (Category1='{0}' or Category2='{0}' or Category3='{0}' or Category4='{0}' or Category5='{0}')", cat)).ContinueWith ((t) => {
				OnCompletion(t.Result);
			});
		}

		/// <summary>
		/// Retrieves all of the categories in the database
		/// </summary>
		/// <returns>The categories.</returns>
		public void GetCategories(Action<List<WPCategory>> OnCompletion)
		{
			QueryAsync<WPCategory> ("select * from WPCategory").ContinueWith ((t) => {
				OnCompletion(t.Result);
			});
		}

		/// <summary>
		/// Adds a post to the Database
		/// </summary>
		/// <param name="post">The post to add to the database</param>
		public void AddPostToDB(WPPost post)
		{
			GetAsync<WPAuthor>(post.Author.ID).ContinueWith((t) => {
				//Console.WriteLine("Author Result");
				if(t.Status == System.Threading.Tasks.TaskStatus.Faulted)
				{
					InsertAsync(post.Author);
					//Console.WriteLine("No existing entry for Author, adding new : " + post.Author.Name);
				}
				else
				{
					UpdateAsync(post.Author);
					//Console.WriteLine("Updating Author : " + post.Author.Name);
				}
			});
			// Add the Author
			/*try 
			{

				if(a != null)
				{
					UpdateAsync (post.Author);
					Console.WriteLine("Updating Author : " + post.Author.Name);
				}
			}
			catch(Exception e)
			{
				// There wasn't an entry
				InsertAsync (post.Author);
				Console.WriteLine("No existing entry for Author, adding new : " + post.Author.Name);
			}*/

			// We ONLY want to add product categories to our list ( we may need blog categories later, but not now...
			if ( post.Type == "product" ) {

				foreach(var kvp in post.Categories)
				{
					// Add/Check on Categories
					GetAsync<WPCategory> (kvp.Value.Name).ContinueWith ((t) => {
						if(t.Status == System.Threading.Tasks.TaskStatus.Faulted)
						{
							InsertAsync(kvp.Value).ContinueWith((t2) => {
								if (NewCategoryAdded != null)
									NewCategoryAdded (kvp.Value);
								Console.WriteLine("No existing entry for Category, adding new : " + kvp.Value.Name);
							});
						}
						else
						{
							UpdateAsync(kvp.Value).ContinueWith((t2) => {
								if(CategoryWasUpdated != null)
									CategoryWasUpdated(kvp.Value);
								Console.WriteLine("Updating Category : " + kvp.Value.Name);
							});
						}
					});


					/*try
					{
						var c = GetAsync<WPCategory>(kvp.Value.Name);
						if(c != null)
						{
							UpdateAsync (kvp.Value);
							if(CategoryWasUpdated != null)
								CategoryWasUpdated(kvp.Value);
							Console.WriteLine("Updating Category : " + kvp.Value.Name);
						}
					}
					catch(Exception e)
					{
						// No entry for the Category
						InsertAsync (kvp.Value);
						if (NewCategoryAdded != null)
							NewCategoryAdded (kvp.Value);
						Console.WriteLine("No existing entry for Category, adding new : " + kvp.Value.Name);
					}*/
				}
			}

			foreach(var tag in post.Tags)
			{
				GetAsync<WPTag> (tag.Value.Name).ContinueWith ((t) => {
					if(t.Status == System.Threading.Tasks.TaskStatus.Faulted)
					{
						InsertAsync(tag.Value).ContinueWith((t2) => {
							//Console.WriteLine("No existing entry for Tag, adding new : " + tag.Value.Name);
						});
					}
					else
					{
						UpdateAsync(tag.Value).ContinueWith((t2) => {
							//Console.WriteLine("Updating Tag : " + tag.Value.Name);
						});
					}
				});
				// Add/Check on Tags
				/*try
				{
					var t = GetAsync<WPTag>(tag.Value.Name);
					if(t != null)
					{
						UpdateAsync(tag.Value);
						Console.WriteLine("Updating Tag : " + tag.Value.Name);
					}
				}
				catch(Exception e)
				{
					// No entry for the Tag
					InsertAsync (tag.Value);
					Console.WriteLine("No existing entry for Tag, adding new : " + tag.Value.Name);
				}*/
			}

			GetAsync<WPPost> (post.ID).ContinueWith ((t) => {
				if(t.Status == System.Threading.Tasks.TaskStatus.Faulted)
				{
					InsertAsync(post).ContinueWith((t2) => {
						if(NewPostAdded != null)
							NewPostAdded (post); 
						//Console.WriteLine("No existing entry.  Adding new : " + post.Title);
					});
				}
				else
				{
					UpdateAsync(post).ContinueWith((t2) => {
						if(PostWasUpdated != null)
							PostWasUpdated(post);
						//Console.WriteLine("Updated Post : " + post.Title);
					});
				}
			});

			/*try
			{
				var p = GetAsync<WPPost>(post.ID);
				if(p != null)
				{
					UpdateAsync(post);
					if(PostWasUpdated != null)
						PostWasUpdated(post);
					Console.WriteLine("Updated Post : " + post.Title);
				}
			}
			catch (Exception e)
			{
				// There wasn't an entry
				InsertAsync(post);
				if(NewPostAdded != null)
					NewPostAdded (post);
				Console.WriteLine("No existing entry.  Adding new : " + post.Title);
			}*/

			//Console.WriteLine("Added post to DB : " + post.Title);
		}
	}
}

