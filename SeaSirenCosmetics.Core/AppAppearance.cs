using System;
#if __ANDROID__
#else
using MonoTouch.UIKit;
#endif

namespace SeaSirenCosmeticsCore
{
	public struct Colour
	{
		public int Red;
		public int Green;
		public int Blue;

		public Colour( int R, int G, int B )
		{
			Red = R;
			Green = G;
			Blue = B;
		} 

	}

	public static class AppAppearance
	{
		public static readonly Colour Background = new Colour(37,37,37);
		public static readonly Colour Seperator = new Colour(99,99,99);
		public static readonly Colour SeaSirenBlue = new Colour(25,139,191);

		#if __ANDROID__
		#else

		public static UIColor UIcolorBackground() {
			return UIColor.FromRGB( Background.Red, Background.Green, Background.Blue );
		}

		public static UIColor UIColorSeperator() {
			return UIColor.FromRGB( Seperator.Red, Seperator.Green, Seperator.Blue );
		}

		public static UIColor UIColorSeaSirenBlue() {
			return UIColor.FromRGB( SeaSirenBlue.Red, SeaSirenBlue.Green, SeaSirenBlue.Blue );
		}

		#endif
	}
}

