using System;
using System.Json;
using SQLite;

namespace WPRestAPI
{
	/// <summary>
	/// WP author class contains all the data related to an Author on the WordPress site.
	/// </summary>
	public class WPAuthor
	{
		[PrimaryKey, Unique]
		/// <summary>
		/// The unique ID of the author (from WordPress)
		/// </summary>
		/// <value>The ID</value>
		public int ID { get; set; }
		/// <summary>
		/// The Email Address of the Author
		/// </summary>
		/// <value>The email address</value>
		public string Email { get; set; }
		/// <summary>
		/// The Name of the Author
		/// </summary>
		/// <value>The name</value>
		public string Name { get; set; }
		/// <summary>
		/// The URL of the Author.
		/// </summary>
		/// <value>The URL</value>
		public string URL { get; set; }
		/// <summary>
		/// The URL of the Author's avatar
		/// </summary>
		/// <value>The avatar URL.</value>
		public string avatar_URL { get; set; }
		/// <summary>
		/// The URL of the Author's profile
		/// </summary>
		/// <value>The profile URL.</value>
		public string profile_URL { get; set; }
	}
}

