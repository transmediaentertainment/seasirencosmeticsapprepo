using System;
using System.Collections.Generic;
using System.Net;
using System.Json;
using RestSharp.Deserializers;
using RestSharp;

namespace WPRestAPI
{
	/// <summary>
	/// Manages content for an entire WordPress site via it's Rest API,
	/// with some custom modiciations that must be administered on the site's plugins
	/// </summary>
	public class WPSite
	{
		/// <summary>
		/// Occurs when content population is completed.
		/// </summary>
		public event Action FinishedPopulationEvent;
		/// <summary>
		/// Occurs when a new post is received.
		/// </summary>
		public event Action<WPPost> NewPostReceived;
		/// <summary>
		/// Gets a value indicating whether this instance is currently populating.
		/// </summary>
		/// <value><c>true</c> if this instance is currently populating; otherwise, <c>false</c>.</value>
		public bool IsPopulating { get; private set; }
		/// <summary>
		/// The Rest Client used to communicate with the Wordpress servers
		/// </summary>
		private readonly RestClient m_Client = new RestClient ("https://public-api.wordpress.com");
		/// <summary>
		/// The URI of the WordPress site we are caching data from.
		/// </summary>
		private Uri m_WPSiteURI;
		/// <summary>
		/// A List of posts from the WordPress site.
		/// </summary>
		private readonly List<WPPost> m_Posts = new List<WPPost> ();
		/// <summary>
		/// Total number of posts found on the site.
		/// </summary>
		private int m_PostTotal;

		/// <summary>
		/// Initializes a new instance of the <see cref="WPRestAPI.WPSite"/> class.
		/// </summary>
		/// <param name="wpSiteURL">The URL of the Site we are going to track</param>
		public WPSite (string wpSiteURL)
		{
			m_WPSiteURI = new Uri (wpSiteURL);
		}

		/// <summary>
		/// Populates all posts.
		/// </summary>
		public void PopulateAllPosts ()
		{
			PopulateAllPosts (null);
		}

		/// <summary>
		/// Populates all posts of a given type
		/// </summary>
		/// <param name="type">Type of posts to populate</param>
		public void PopulateAllPosts (string type)
		{
			if (IsPopulating)
				return; // Should probably throw an error here

			ContinuePopulatingAllPosts (0, type);
		}

		/// <summary>
		/// Number of responses received from the site.
		/// </summary>
		private int m_Responses = 0;
		/// <summary>
		/// Continues the populating all posts.
		/// </summary>
		/// <param name="offset">Offset from the start of posts</param>
		/// <param name="type">Type of posts to populate</param>
		private void ContinuePopulatingAllPosts(int offset, string type)
		{
			var request = new RestRequest("rest/v1/sites/" + m_WPSiteURI.Host + "/posts/");
			if(type != null)
				request.AddParameter("type", type);
			request.AddParameter("number", 30);
			request.AddParameter("offset", offset);
			m_Client.ExecuteAsync(request, (response) => {
				Console.WriteLine("Received Response : " + m_Responses);
				m_Responses++;
				HandlePostsResponse(response, offset, type);
			});
		}

		/// <summary>
		/// Handles the response of a posts request
		/// </summary>
		/// <param name="response">Response from the server</param>
		/// <param name="offset">Offset from the start of the posts</param>
		/// <param name="type">Type of posts that were requested</param>
		private void HandlePostsResponse(IRestResponse response, int offset, string type)
		{
			var j = new JsonDeserializer();
			WPPosts posts;
			try
			{
				posts = j.Deserialize<WPPosts>(response);
			}
			catch(Exception e) 
			{
				Console.WriteLine ("Excpetion when getting WPPosts : " + e.Message + " : " + response.Content);
				return;
			}
			m_PostTotal = posts.Found;
			if (posts.Posts != null) {
				Console.WriteLine ("Found Posts : " + posts.Found + " in query : " + posts.Posts.Count);
				for (int i = 0; i < posts.Posts.Count; i++) {
					m_Posts.Add (posts.Posts [i]);
					Console.WriteLine ("POST T:" + posts.Posts[i].Type + " N:" + posts.Posts [i].Title + " CS:" + posts.Posts [i].Categories + " C1:" + posts.Posts [i].Category1 + " C2:" + posts.Posts [i].Category2);
					if (NewPostReceived != null)
						NewPostReceived (posts.Posts [i]);
					offset++;
				}
			}
			if(offset < m_PostTotal - 1)
			{
				ContinuePopulatingAllPosts(offset, type);
			}
			else if(FinishedPopulationEvent != null)
				FinishedPopulationEvent();
		}
	}
}