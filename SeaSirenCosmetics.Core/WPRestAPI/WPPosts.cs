using System;
using System.Collections.Generic;

namespace WPRestAPI
{
	/// <summary>
	/// Wrapper class for deserializing a collection of posts.
	/// </summary>
	public class WPPosts
	{
		/// <summary>
		/// Number of posts found
		/// </summary>
		/// <value>Found Posts.</value>
		public int Found { get; set; }
		/// <summary>
		/// List of Posts from the site.
		/// </summary>
		/// <value>The posts.</value>
		public List<WPPost> Posts { get; set; }
	}
}

