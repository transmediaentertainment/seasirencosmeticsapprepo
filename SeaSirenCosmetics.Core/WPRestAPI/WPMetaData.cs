using System;
using SQLite;

namespace WPRestAPI
{
	/// <summary>
	/// WP meta data class, stores WordPress Meta Data associated with posts.
	/// </summary>
	public class WPMetaData
	{
		// The SQL Parser ignores this, the JSON parser uses it.
		[Ignore]
		/// <summary>
		/// The Wordpress based ID
		/// </summary>
		/// <value>The ID</value>
		public string ID 
		{ 
			get { return m_ID.ToString(); }
			set 
			{
				if(!int.TryParse(value, out m_ID))
				{
					// TODO : something about errors
				}
			}
		}

		[PrimaryKey]
		/// <summary>
		/// Gets or sets the meta ID
		/// </summary>
		/// <value>The meta ID</value>
		public int MetaID 
		{ 
			get { return m_ID; }
			set { m_ID = value; } 
		}

		/// <summary>
		/// Local private stored aof the Wordpress based ID
		/// </summary>
		private int m_ID;
		/// <summary>
		/// The Meta Data's Key
		/// </summary>
		/// <value>The key</value>
		public string Key { get; set; }
		/// <summary>
		///  The Meta Data's Value
		/// </summary>
		/// <value>The value.</value>
		public string Value { get; set; }
	}
}

