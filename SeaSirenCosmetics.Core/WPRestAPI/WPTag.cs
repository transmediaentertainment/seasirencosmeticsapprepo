using System;

namespace WPRestAPI
{
	/// <summary>
	/// WP Tag class.  Only used for JSON deserialization compliance.
	/// </summary>
	public class WPTag : WPSorter
	{
	}
}

