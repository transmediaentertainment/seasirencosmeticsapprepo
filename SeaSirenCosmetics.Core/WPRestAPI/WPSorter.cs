using System;
using System.Json;
using SQLite;

namespace WPRestAPI
{
	/// <summary>
	/// Base class for tag and category, which are used for sorting.
	/// </summary>
	public abstract class WPSorter
	{
		[PrimaryKey]
		/// <summary>
		/// The name of the Tag/Category
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }
		/// <summary>
		/// The Slug URL for the Tag/Category
		/// </summary>
		/// <value>The slug.</value>
		public string Slug { get; set; }
		/// <summary>
		/// The Description of the Tag/Category
		/// </summary>
		/// <value>The description.</value>
		public string Description { get; set; }
		/// <summary>
		/// Number of posts in the Tag/Category
		/// </summary>
		/// <value>The post count.</value>
		public int PostCount { get; set; }

		// Meta is missing.....
	}
}

