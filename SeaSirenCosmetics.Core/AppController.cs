using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LocalDataCaching;
using WPRestAPI;

using HtmlAgilityPack;
using System.Web;

namespace SeaSirenCosmeticsCore
{
	/// <summary>
	/// Main Application controller used across both iOS and Android platforms.
	/// </summary>
	public class AppController
	{
		/// <summary>
		/// Private static reference to the instance (Singleton Pattern)
		/// </summary>
		private static AppController m_Instance;
		/// <summary>
		/// Public access to the static instance of the AppController (Singleton Pattern)
		/// </summary>
		/// <value>The instance of the App Controller.</value>
		public static AppController Instance 
		{
			get 
			{
				if (m_Instance == null)
					m_Instance = new AppController ();
				return m_Instance;
			}
		}

		/// <summary>
		/// The website the app is fetching data from.
		/// </summary>
		private WPSite m_FetchSite;
		/// <summary>
		/// Reference to the Product Cache stored locally.
		/// </summary>
		/// <value>The product cache stroed on disk.</value>
		public ProductCache Products { get; private set; }

		/// <summary>
		/// Occurs when there is a new blog post in the cache.
		/// </summary>
		public event Action<WPPost> NewBlogPostInCacheEvent;
		/// <summary>
		/// Occurs when a blog post in the cache is updated.
		/// </summary>
		public event Action<WPPost> BlogPostInCacheUpdatedEvent;
		/// <summary>
		/// Occurs when a new product post is added to the cache.
		/// </summary>
		public event Action<WPPost> NewProductPostInCacheEvent;
		/// <summary>
		/// Occurs when a product post in the cache is updated.
		/// </summary>
		public event Action<WPPost> ProductPostInCacheUpdatedEvent;

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsCore.AppController"/> class.
		/// Private to conform to the Singleton Pattern.
		/// </summary>
		private AppController()
		{
			// Initialize the Product cache
			Products = new ProductCache ();
			// Check for Updates
			m_FetchSite = new WPSite ("http://dev.seasiren.com.au");
			m_FetchSite.PopulateAllPosts ();
			m_FetchSite.FinishedPopulationEvent += HandleBlogPopulationComplete;
			m_FetchSite.NewPostReceived += HandleNewPostReceived;

			// Register for Product post updates
			Products.NewPostAdded += HandleNewPostInCache;
			Products.PostWasUpdated += HandlePostInCacheUpdated;
		}

		/// <summary>
		/// Handles a new post being received.
		/// </summary>
		/// <param name="post">The new post that was received</param>
		private void HandleNewPostReceived (WPPost post)
		{
			post.Title = ReformatHTML (post.Title);
			post.Excerpt = ReformatHTML (post.Excerpt);
			post.Content = ReformatHTML (post.Content);

			// All of the category strings need to be reformatted - we are using the kvp.Value.Name for our list, so this one is the most important.
			// I'm a little confused why there are so many of these... and what they may be used for...
			// so for the moment, I'm processing all of them, they should all be identical
			Dictionary<string, WPCategory> updatedDictionary = new Dictionary<string, WPCategory> ();
			foreach (var kvp in post.Categories) {
				kvp.Value.Name = ReformatCategory (kvp.Value.Name);
				updatedDictionary.Add (ReformatCategory (kvp.Key), kvp.Value);
			}
			post.Categories = updatedDictionary;

			post.Category1 = ReformatCategory (post.Category1);
			post.Category2 = ReformatCategory (post.Category2);
			post.Category3 = ReformatCategory (post.Category3);
			post.Category4 = ReformatCategory (post.Category4);
			post.Category5 = ReformatCategory (post.Category5);

			// Add the post to the Database
			Products.AddPostToDB(post);

			if(post.FeaturedImage != "")
				ImageCache.CacheImageFromURL(post.FeaturedImage);
			if(post.ProductImageGallery != null)
			{
				var split = post.ProductImageGallery.Split(',');
				foreach(var s in split)
				{
					if(s != "")
						ImageCache.CacheImageFromURL(s);
				}
			}
		}

		/// <summary>
		/// Reformats the Category string into a format and size able to be displayed in the App UI
		/// We look up some of the longer strings and shorten them. This will need to be updated if new categories are addon on the website (that are too long).
		/// in that case, we will do some generic truncation of long strings to ensure it always looks as good as it can, and strings don't overflow their space...
		/// </summary>
		/// <returns>reformated category string</returns>
		/// <param name="categoryString">Category string.</param>
		private string ReformatCategory(string categoryString)
		{
			// First do the normal HTML reformatting to get rid of pesky HTML symbols (like &amp for example)
			string reformattedString = ReformatHTML( categoryString );

			// Next we look up known long strings and convert to shorter nicer forms
			switch (reformattedString) {
			case "2013 Launch Collections":
				reformattedString = "Launch Collections";
				break;
			case "Swarovski® Crystal Glass Travel Nail Files":
				reformattedString = "Swarovski® Files";
				break;
			case "Swarovski® Manicure Travel Sets":
				reformattedString = "Swarovski® Manicure";
				break;
			case "Hard Case Glass Travel Nail File":
			case "Hard Case Glass Travel Nail Files":
				reformattedString = "Hard Case Files";
				break;
			}

			// Finally, we still do a length check and automatically truncate incase there are some new categories we have not yet handled
			if (reformattedString.Length > 20) {
				reformattedString = reformattedString.Substring (0, 20);
			}
			return reformattedString;
		}

		/// <summary>
		/// Reformats the HTML into a more usable format for displaying in UI.
		/// </summary>
		/// <returns>The reformatted HTML</returns>
		/// <param name="htmlString">The Html string to reformat</param>
		private string ReformatHTML(string htmlString)
		{
			var doc = new HtmlDocument ();
			doc.LoadHtml (htmlString);
			// Get all BR nodes and replace them with new line characters
			var nodes = doc.DocumentNode.SelectNodes ("//br");
			if (nodes != null) 
			{
				foreach (var node in nodes.ToArray()) 
				{
					var replacement = doc.CreateTextNode ("\n");
					node.ParentNode.ReplaceChild (replacement, node);
				}
			}
			// Get all P nodes and replace them with new line characters
			nodes = doc.DocumentNode.SelectNodes ("//p");
			if (nodes != null) 
			{
				foreach (var node in nodes.ToArray()) 
				{
					node.ParentNode.ReplaceChild (HtmlTextNode.CreateNode(node.InnerText + "\n"), node);
				}
			}
			// Decode and return the html.
			var decode = HttpUtility.HtmlDecode (doc.DocumentNode.InnerText);
			return decode;
		}

		/// <summary>
		/// Handles a post in the cache being updated.
		/// </summary>
		/// <param name="post">The post that has been updated</param>
		private void HandlePostInCacheUpdated(WPPost post)
		{
			// Relay the event based on whether it is a blog post or product.
			if (post.Type == "product") 
			{
				if (ProductPostInCacheUpdatedEvent != null)
					ProductPostInCacheUpdatedEvent (post);
			} 
			else 
			{
				if (BlogPostInCacheUpdatedEvent != null)
					BlogPostInCacheUpdatedEvent (post);
			}
		}

		/// <summary>
		/// Handles a new post being added to the cache.
		/// </summary>
		/// <param name="post">Post.</param>
		private void HandleNewPostInCache(WPPost post)
		{
			// Relay the event based on whether it is a blog post or product.
			if (post.Type == "product") 
			{
				if (NewProductPostInCacheEvent != null)
					NewProductPostInCacheEvent (post);
			}
			else
			{
				if (NewBlogPostInCacheEvent != null)
					NewBlogPostInCacheEvent (post);
			}
		}

		/// <summary>
		/// Handles the blog population complete event.
		/// </summary>
		private void HandleBlogPopulationComplete()
		{
			m_FetchSite.FinishedPopulationEvent -= HandleBlogPopulationComplete;
			m_FetchSite.PopulateAllPosts ("product");
			m_FetchSite.FinishedPopulationEvent += HandleProductPopulationComplete;
		}

		/// <summary>
		/// Handles the product population complete event.
		/// </summary>
		private void HandleProductPopulationComplete()
		{
			m_FetchSite.FinishedPopulationEvent -= HandleProductPopulationComplete;
		}

		/// <summary>
		/// Gets the blog posts from the cache
		/// </summary>
		/// <returns>The blog posts currently in the Product Cache</returns>
		public void GetBlogPosts(Action<List<WPPost>> OnCompletion)
		{
			Products.GetBlogPosts (OnCompletion);
		}
	}
}

