using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SeaSirenCosmeticsCore;

namespace SeaSirenCosmeticsiOS
{
	/// <summary>
	/// Blog table view cell.
	/// </summary>
	public partial class BlogTableViewCell : UITableViewCell
	{
		/// <summary>
		/// Reference to the Nib file defining the table view cell
		/// </summary>
		public static readonly UINib Nib = UINib.FromName ("BlogTableViewCell", NSBundle.MainBundle);

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsiOS.BlogTableViewCell"/> class.
		/// </summary>
		/// <param name="handle">Handle.</param>
		public BlogTableViewCell (IntPtr handle) : base (handle)
		{
			this.BackgroundColor = AppAppearance.UIcolorBackground ();
		}

		/// <summary>
		/// Creates an instance of the table view cell
		/// </summary>
		public static BlogTableViewCell Create ()
		{
			return (BlogTableViewCell)Nib.Instantiate (null, null) [0];
		}

		/// <summary>
		/// Sets the content and image of the Table View Cell
		/// </summary>
		/// <param name="image">Image to set</param>
		/// <param name="contentString">Content string to set</param>
		public void SetImageAndContent(UIImage image, string contentString)
		{
			//this.imageFeature.ContentMode = UIViewContentMode.ScaleAspectFit;
			//this.imageFeature.ClipsToBounds = true;
			if (image != null) {
				// Set the image
				this.imageFeature.Image = image;
			} else {
				var path = NSBundle.MainBundle.PathForResource ("DefaultLogoImage", "png");
				this.imageFeature.Image = new UIImage (path);
			}

			this.textContent.Text = contentString;
		}
	}
}

