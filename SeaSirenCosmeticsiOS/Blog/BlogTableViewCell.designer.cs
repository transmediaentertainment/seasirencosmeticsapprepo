// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace SeaSirenCosmeticsiOS
{
	[Register ("BlogTableViewCell")]
	partial class BlogTableViewCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel textContent { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imageFeature { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (textContent != null) {
				textContent.Dispose ();
				textContent = null;
			}

			if (imageFeature != null) {
				imageFeature.Dispose ();
				imageFeature = null;
			}
		}
	}
}
