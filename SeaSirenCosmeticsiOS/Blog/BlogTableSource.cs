using System;
using MonoTouch.UIKit;
using WPRestAPI;
using System.Collections.Generic;
using LocalDataCaching;

namespace SeaSirenCosmeticsiOS
{
	/// <summary>
	/// Blog table source.  Provides the data for the Table View for the blog
	/// </summary>
	public class BlogTableSource : UITableViewSource
	{
		/// <summary>
		/// The posts to display in the blog
		/// </summary>
		private List<WPPost> m_Posts;
		/// <summary>
		/// The cell identifier for blog cells
		/// </summary>
		private const string CellIdentifier = "BlogTableViewCell";
		/// <summary>
		/// The height of the blog table cells
		/// </summary>
		private const float BlogTableCellHeight = 100f;

		/// <summary>
		/// Occurs when post selected event.
		/// </summary>
		public event Action<WPPost> PostSelectedEvent;

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsiOS.BlogTableSource"/> class.
		/// </summary>
		/// <param name="posts">Posts to populate the view with</param>
		public BlogTableSource(WPPost[] posts)
		{
			m_Posts = new List<WPPost> (posts);
		}

		/// <Docs>Table view displaying the rows.</Docs>
		/// <summary>
		/// Called by the OS when determiing the number of rows in a section
		/// </summary>
		/// <returns>The rows in section.</returns>
		/// <param name="tableview">Tableview.</param>
		/// <param name="section">Section to get rows for</param>
		public override int RowsInSection (UITableView tableview, int section)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			return m_Posts.Count;
		}

		/// <Docs>Table view requesting the cell.</Docs>
		/// <summary>
		/// Gets the requested cell.
		/// </summary>
		/// <returns>The cell.</returns>
		/// <param name="tableView">Table view.</param>
		/// <param name="indexPath">Index path for the cell</param>
		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 

			BlogTableViewCell cell = (BlogTableViewCell)tableView.DequeueReusableCell (CellIdentifier);

			if (cell == null)
				cell = BlogTableViewCell.Create ();
			var post = m_Posts [indexPath.Row];
			UIImage img = null;
			if (post.FeaturedImage != null && post.FeaturedImage != "") 
			{
				var path = ImageCache.GetImagePathForCache (post.FeaturedImage);
				// The path is null here is we haven't downloaded the image yet.
				if(path != null)
					img = new UIImage (path);
			}

			var content = post.Title + "\n" + post.Excerpt;

			cell.SetImageAndContent (img, content);
			return cell;
		}

		/// <Docs>Table view.</Docs>
		/// <summary>
		/// Gets the height for row.
		/// </summary>
		/// <returns>The height for row.</returns>
		/// <param name="tableView">Table view.</param>
		/// <param name="indexPath">Index path for the row</param>
		public override float GetHeightForRow (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			return BlogTableCellHeight;
		}

		/// <Docs>Table view containing the row.</Docs>
		/// <summary>
		/// Called when a row is selected by the user
		/// </summary>
		/// <param name="tableView">Table view.</param>
		/// <param name="indexPath">Index path to the selected row</param>
		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			if (PostSelectedEvent != null)
				PostSelectedEvent (m_Posts[indexPath.Row]);
			tableView.DeselectRow(indexPath, true);
		}

		/// <summary>
		/// Adds the new post.
		/// </summary>
		/// <param name="post">Post.</param>
		public void AddNewPost(WPPost post)
		{
			m_Posts.Add (post);
		}
	}
}

