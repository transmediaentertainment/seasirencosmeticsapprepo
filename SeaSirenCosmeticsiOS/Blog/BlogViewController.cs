using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SeaSirenCosmeticsCore;
using WPRestAPI;

namespace SeaSirenCosmeticsiOS
{
	/// <summary>
	/// Blog view controller.
	/// </summary>
	public partial class BlogViewController : UITableViewController
	{
		/// <summary>
		/// Gets a value indicating whether this <see cref="SeaSirenCosmeticsiOS.BlogViewController"/> user interface idiom is phone.
		/// </summary>
		/// <value><c>true</c> if user interface idiom is phone; otherwise, <c>false</c>.</value>
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsiOS.BlogViewController"/> class.
		/// </summary>
		/// <param name="handle">Handle.</param>
		public BlogViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		#region View lifecycle

		/// <summary>
		/// The source of the blog table's data
		/// </summary>
		private BlogTableSource m_Source;
		/// <summary>
		/// The currently selected post
		/// </summary>
		private WPPost m_SelectedPost;

		/// <summary>
		/// Called by the OS when the view has loaded
		/// </summary>
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = NSBundle.MainBundle.LocalizedString ("Ebb & Flo", "Ebb & Flo");
			this.View.BackgroundColor = AppAppearance.UIcolorBackground ();
			TableView.SeparatorColor = AppAppearance.UIColorSeperator ();

			// Perform any additional setup after loading the view, typically from a nib.
			AppController.Instance.Products.GetBlogPosts ((posts) => {
				InvokeOnMainThread(new NSAction(() => {
					if (posts != null)
						m_Source = new BlogTableSource (posts.ToArray());
					else
						m_Source = new BlogTableSource (new WPPost[] {});
					TableView.Source = m_Source;

					AppController.Instance.NewBlogPostInCacheEvent += HandleNewBlogPost;
					m_Source.PostSelectedEvent += HandlePostSelected;
				}));
			});
		}

		/// <summary>
		/// Called by the OS before the view appears
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		#endregion

		/// <summary>
		/// Called by the OS to determine if the screen should rotate to the orientation
		/// </summary>
		/// <returns><c>true</c>, if autorotate to interface orientation was shoulded, <c>false</c> otherwise.</returns>
		/// <param name="toInterfaceOrientation">To interface orientation.</param>
		/*
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			if (UserInterfaceIdiomIsPhone) {
				return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
			} else {
				return true;
			}
		}
		*/

		/// <summary>
		/// Handles a post being selected.
		/// </summary>
		/// <param name="post">The selected post</param>
		private void HandlePostSelected(WPPost post)
		{
			m_SelectedPost = post;
			PerformSegue ("SinglePushSegue", this);
		}

		/// <summary>
		/// Called by the OS before a storyboard segue begins
		/// </summary>
		/// <param name="segue">Segue.</param>
		/// <param name="sender">Sender.</param>
		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);
			if (segue.Identifier == "SinglePushSegue") 
			{
				var blogPostController = segue.DestinationViewController as SinglePostBlogViewController;
				if (blogPostController != null) 
				{
					blogPostController.PopulatePost (m_SelectedPost);
				}
			}
		}

		/// <summary>
		/// Handles a new blog post being added to the Database
		/// </summary>
		/// <param name="post">Post.</param>
		private void HandleNewBlogPost(WPPost post)
		{
			Console.WriteLine ("Got new blog post");
			BeginInvokeOnMainThread (new NSAction (() => {
				m_Source.AddNewPost (post);
				TableView.ReloadData();
			}));

		}
	}
}

