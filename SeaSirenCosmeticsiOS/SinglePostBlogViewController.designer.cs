// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace SeaSirenCosmeticsiOS
{
	[Register ("SinglePostBlogViewController")]
	partial class SinglePostBlogViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIScrollView scrollView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel titleLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView featuredImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel contentLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (scrollView != null) {
				scrollView.Dispose ();
				scrollView = null;
			}

			if (titleLabel != null) {
				titleLabel.Dispose ();
				titleLabel = null;
			}

			if (featuredImage != null) {
				featuredImage.Dispose ();
				featuredImage = null;
			}

			if (contentLabel != null) {
				contentLabel.Dispose ();
				contentLabel = null;
			}
		}
	}
}
