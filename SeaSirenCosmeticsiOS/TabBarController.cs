using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace SeaSirenCosmeticsiOS
{
	public partial class TabBarController : UITabBarController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}
		public TabBarController (IntPtr handle) : base (handle)
		{



			UITabBarItem[] tabbaritems = this.TabBar.Items;
			Console.WriteLine ("TAB BAR ITEMS: " + tabbaritems.Length.ToString() );
			if (tabbaritems.Length != 4) {
				throw new Exception ("Invalid Number of Tabs: " + tabbaritems.Length);
			}

			tabbaritems[0].Title = "Ebb & Flow";
			tabbaritems[1].Title = "Catalog";
			tabbaritems[2].Title = "Try It On";
			tabbaritems[3].Title = "About";

			if ( UIDevice.CurrentDevice.CheckSystemVersion(7,0) ) {
					
				// SetFinishedImages is depricated in iOS7... use this instead
				tabbaritems [0].Image = UIImage.FromBundle ("tabbar_icon_blog").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [0].SelectedImage = UIImage.FromBundle ("tabbar_icon_blog_selected").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [1].Image = UIImage.FromBundle ("tabbar_icon_catalog").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [1].SelectedImage = UIImage.FromBundle ("tabbar_icon_catalog_selected").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [2].Image = UIImage.FromBundle ("tabbar_icon_tryiton").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [2].SelectedImage = UIImage.FromBundle ("tabbar_icon_tryiton_selected").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [3].Image = UIImage.FromBundle ("tabbar_icon_about").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
				tabbaritems [3].SelectedImage = UIImage.FromBundle ("tabbar_icon_about_selected").ImageWithRenderingMode (UIImageRenderingMode.AlwaysOriginal);
			} else {
				#pragma warning disable 618
				tabbaritems [0].SetFinishedImages (UIImage.FromBundle ("tabbar_icon_blog_selected"), UIImage.FromBundle ("tabbar_icon_blog"));
				tabbaritems [1].SetFinishedImages (UIImage.FromBundle ("tabbar_icon_catalog_selected"), UIImage.FromBundle ("tabbar_icon_catalog"));
				tabbaritems [2].SetFinishedImages (UIImage.FromBundle ("tabbar_icon_tryiton_selected"), UIImage.FromBundle ("tabbar_icon_tryiton"));
				tabbaritems [3].SetFinishedImages (UIImage.FromBundle ("tabbar_icon_about_selected"), UIImage.FromBundle ("tabbar_icon_about"));
				#pragma warning restore 618
			}

		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
	}
}

