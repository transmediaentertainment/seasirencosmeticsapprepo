// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace SeaSirenCosmeticsiOS
{
	[Register ("ProductCatalogueViewController")]
	partial class ProductCatalogueViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIPickerView categoryPicker { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField categoryTextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView tableView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (categoryPicker != null) {
				categoryPicker.Dispose ();
				categoryPicker = null;
			}

			if (categoryTextField != null) {
				categoryTextField.Dispose ();
				categoryTextField = null;
			}

			if (tableView != null) {
				tableView.Dispose ();
				tableView = null;
			}
		}
	}
}
