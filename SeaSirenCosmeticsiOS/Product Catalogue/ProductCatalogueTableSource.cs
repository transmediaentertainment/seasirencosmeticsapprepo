using System;
using System.Collections.Generic;
using WPRestAPI;
using MonoTouch.UIKit;
using LocalDataCaching;
using SeaSirenCosmetics.Core;

namespace SeaSirenCosmeticsiOS
{
	/// <summary>
	/// Product catalogue table source.
	/// </summary>
	public class ProductCatalogueTableSource : UITableViewSource
	{
		/// <summary>
		/// Posts currently in the product list
		/// </summary>
		private List<WPPost> m_Posts;
		/// <summary>
		/// The cell identifier for the table cells
		/// </summary>
		private const string CellIdentifier = "BlogTableViewCell";
		/// <summary>
		/// The height of the blog table cell.
		/// </summary>
		private const float BlogTableCellHeight = 100f;

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsiOS.ProductCatalogueTableSource"/> class.
		/// </summary>
		/// <param name="posts">Posts.</param>
		public ProductCatalogueTableSource (List<WPPost> posts)
		{
			if (posts == null)
				m_Posts = new List<WPPost> ();
			else
				m_Posts = posts;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			return m_Posts.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			BlogTableViewCell cell = (BlogTableViewCell)tableView.DequeueReusableCell (CellIdentifier);

			if (cell == null)
				cell = BlogTableViewCell.Create ();
			cell.UserInteractionEnabled = false;
			var post = m_Posts [indexPath.Row];
			UIImage img = null;
			if (post.FeaturedImage != null && post.FeaturedImage != "") 
			{
				var ttImage = new TTImage (post.FeaturedImage);
				var scale = MonoTouch.UIKit.UIScreen.MainScreen.Scale;
				Console.WriteLine("Screen scale is : " + scale);
				var wrap = ttImage.GetThumbnail ((int)(90 * scale), (int)(90 * scale));
				if (wrap != null)
					img = wrap.Image;
				//var path = ImageCache.GetImagePathForCache (post.FeaturedImage);
				// The path is null here is we haven't downloaded the image yet.
				//if(path != null)
					//img = new UIImage (path);
			}

			var content = post.Title;

			cell.SetImageAndContent (img, content);
			return cell;
		}

		public override float GetHeightForRow (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			return BlogTableCellHeight;
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			tableView.DeselectRow(indexPath, true);
		}

		/// <summary>
		/// Replaces the current list of posts with new posts
		/// </summary>
		/// <param name="posts">Posts.</param>
		public void SetNewPosts(List<WPPost> posts)
		{
			m_Posts = posts;
		}
	}
}

