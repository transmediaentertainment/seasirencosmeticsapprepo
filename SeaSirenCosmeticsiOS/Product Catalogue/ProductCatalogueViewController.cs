using System;
using System.Drawing;
using LocalDataCaching;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SeaSirenCosmeticsCore;
using WPRestAPI;

namespace SeaSirenCosmeticsiOS
{
	public partial class ProductCatalogueViewController : UIViewController
	{
		private ProductCatalogueTableSource m_Source;

		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public ProductCatalogueViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		#region View lifecycle
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = NSBundle.MainBundle.LocalizedString ("Catalog", "Catalog");
			this.View.BackgroundColor = AppAppearance.UIcolorBackground ();
			this.tableView.BackgroundColor = AppAppearance.UIcolorBackground ();
			this.tableView.SeparatorColor = AppAppearance.UIColorSeperator ();
			categoryTextField.Text = "Categories";
			categoryTextField.TextColor = AppAppearance.UIColorSeaSirenBlue ();


			m_Source = new ProductCatalogueTableSource (null);

			tableView.Source = m_Source;
			
			// Perform any additional setup after loading the view, typically from a nib.
			categoryTextField.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return true; 
			};

			AppController.Instance.Products.GetCategories ((categories) => {
				InvokeOnMainThread(new NSAction(() => {
					categoryPicker.Model = new CategoryPickerViewModel (categories);
					categoryTextField.InputView = categoryPicker;

					var accessoryView = new UIToolbar (new RectangleF(0f, 0f, 320f, 44f));
					accessoryView.BarStyle = UIBarStyle.Black;
					var space = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);
					var done = new UIBarButtonItem (UIBarButtonSystemItem.Done, (sender, e) => {
						var model = categoryPicker.Model as CategoryPickerViewModel;
						if(model != null)
							SetCategorySelection(model.SelectedCategory);
					});

					accessoryView.Items = new UIBarButtonItem[] { space, done };
					categoryTextField.InputAccessoryView = accessoryView;
				}));
			});
		}

		private void SetCategorySelection(WPCategory category)
		{
			// Set the category
			if (category == null)
				return;

			// We actaully NEVER set the categoryTextField... we use the Title of the post instead...
			//categoryTextField.Text = category.Name;
			Title = category.Name;

			AppController.Instance.Products.GetProductsInCategory (category.Name, (catPosts) => {
				InvokeOnMainThread(new NSAction(() => {
					m_Source.SetNewPosts(catPosts);
					tableView.ReloadData ();
				}));
			});

			// Hide the picker
			categoryTextField.ResignFirstResponder ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		#endregion

		/// <summary>
		/// Called by the OS to determine if the screen should rotate to the orientation
		/// </summary>
		/// <returns><c>true</c>, if autorotate to interface orientation was shoulded, <c>false</c> otherwise.</returns>
		/// <param name="toInterfaceOrientation">To interface orientation.</param>
		/*
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			if (UserInterfaceIdiomIsPhone) {
				return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
			} else {
				return true;
			}
		}
		*/
	}
}

