using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using WPRestAPI;

namespace SeaSirenCosmeticsiOS
{
	/// <summary>
	/// Model class for the Category picker for the product catalogue
	/// </summary>
	public class CategoryPickerViewModel : UIPickerViewModel
	{
		/// <summary>
		/// The current list of categories
		/// </summary>
		private List<WPCategory> m_Categories;
		/// <summary>
		/// The currently selected category
		/// </summary>
		/// <value>The selected category.</value>
		public WPCategory SelectedCategory { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="SeaSirenCosmeticsiOS.CategoryPickerViewModel"/> class.
		/// </summary>
		/// <param name="categories">Categories to use</param>
		public CategoryPickerViewModel (List<WPCategory> categories)
		{
			if (categories == null)
				m_Categories = new List<WPCategory> ();
			else
				m_Categories = categories;
		}

		public override int GetRowsInComponent (UIPickerView picker, int component)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			if(NoItem())
				return 1;
			return m_Categories.Count;
		}

		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			if (NoItem (row))
				return "";
			return m_Categories [row].Name;
		}

		public override void Selected (UIPickerView picker, int row, int component)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			if (NoItem (row))
				SelectedCategory = new WPCategory ();
			else
				SelectedCategory = m_Categories [row];
		}

		public override int GetComponentCount (UIPickerView picker)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			return 1;
		}

		/// <summary>
		/// Are there any categories in our list?
		/// </summary>
		/// <returns><c>true</c>, if item was noed, <c>false</c> otherwise.</returns>
		/// <param name="row">Row.</param>
		private bool NoItem(int row = 0)
		{
			return m_Categories == null || row >= m_Categories.Count;
		}
	}
}

