using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using SeaSirenCosmeticsCore;

namespace SeaSirenCosmeticsiOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		
		public override UIWindow Window {
			get;
			set;
		}

		// This method is called only from FinishedLaunching below
		// It is used to set up our custom bitmaps for all of the views, nav bars etc.
		private void customizeUI()
		{
			Console.WriteLine ("CustomizeUI");

			// Create resizable images
			UIImage navbarbg44Image = UIImage.FromBundle ("navbar_bg_44");
			UIImage navbarbg32Image = UIImage.FromBundle ("navbar_bg_32");
			UIImage tabbarbg49Image = UIImage.FromBundle ("tabbar_bg_49");
			UIImage tabbarselImage = UIImage.FromBundle ("tabbar_icon_select");
			navbarbg44Image.CreateResizableImage(new UIEdgeInsets(2,2,2,2), UIImageResizingMode.Stretch );
			navbarbg32Image.CreateResizableImage(new UIEdgeInsets(2,2,2,2), UIImageResizingMode.Stretch );
			tabbarbg49Image.CreateResizableImage(new UIEdgeInsets(2,2,2,2), UIImageResizingMode.Stretch );
			tabbarselImage.CreateResizableImage(new UIEdgeInsets(2,2,2,2), UIImageResizingMode.Stretch );

			// Set the background image for *all* UINavigationBars
			UINavigationBar.Appearance.SetBackgroundImage (navbarbg44Image, UIBarMetrics.Default);
			UINavigationBar.Appearance.SetBackgroundImage (navbarbg32Image, UIBarMetrics.LandscapePhone);

			// Customize the title text for *all* UINavigationBars
			UITextAttributes navbarattributes = new UITextAttributes ();
			navbarattributes.TextColor = UIColor.White;
			navbarattributes.TextShadowColor = AppAppearance.UIcolorBackground ();
			navbarattributes.TextShadowOffset = new UIOffset (0, 0);
			navbarattributes.Font = UIFont.FromName ("Arial", 0f);

			UINavigationBar.Appearance.SetTitleTextAttributes (navbarattributes);

			UITabBar.Appearance.BackgroundImage = tabbarbg49Image;
			UITabBar.Appearance.SelectionIndicatorImage = tabbarselImage;
			UITabBar.Appearance.TintColor = UIColor.FromRGB (0, 0, 0);
			UITabBar.Appearance.SelectedImageTintColor = AppAppearance.UIColorSeaSirenBlue ();

			UITabBarItem.Appearance.SetTitleTextAttributes(navbarattributes,UIControlState.Normal);

		}

		// This method is invoked after the App finishes launching, before any views are created
		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events 
			customizeUI ();

			return true;
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}
		// This method is called when the application is about to terminate. Save data, if needed. 
		public override void WillTerminate (UIApplication application)
		{
		}
	}
}

